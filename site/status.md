---
layout: default
title: Status
permalink: /status/
---

### Aims

My initial aim was to proof the concept, but now I am trying to make it stable. I had to rewrite almost 70% code in order to make it testable. I tryied to leave only core functions, and it will take some time to get others back.

### Help the project

If you want to make contribution to the project, here you can check how you can do it.

### When are new versions published

Now for publishing new versions CI/CD is configured. That means that each change will trigger pipeline for publishing new snapshot and it is completely automated. There will be 'freeze-points', which will go into release. Each snapshot is available for [downloading](https://gitlab.com/ultrablox/ultraoutliner/-/tags).

### Feedback

If you found a bug or want to request new functions, please use [issue tracker](https://gitlab.com/ultrablox/ultraoutliner/issues) instead of direct contacting me. It will guarantee, that the request will not be forgotten or lost. Issue tracker is in free access, but you need to create a gitlab account in order to create new tickets.
