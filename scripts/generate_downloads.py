#!/usr/bin/env python3

import json
import requests
import gitlab
import logging
import argparse
import arrow
from urllib.parse import urlparse
import os


def main():
  logging.basicConfig(level=logging.INFO)

  parser = argparse.ArgumentParser(description='Generate download page')
  parser.add_argument('--api_key', type=str, required=True)
  args = parser.parse_args()

  gl = gitlab.Gitlab('https://gitlab.com', args.api_key)
  gl.auth()

  project = gl.projects.get('ultrablox/ultraoutliner')
  tags = project.tags.list()

  logging.info('Found {} tags: {}'.format(len(tags), tags))

  downloadsFile = open('../site/download.html', 'w+')

  downloadsFile.write('---\n'
  'layout: default\n'
  '---\n'
  '\n'
  '<div class="container">\n'
  '  <div class="section">\n'
  '    <h1 class="header center teal-text text-lighten-3">Download</h1>\n'
  '  </div>\n'
  )

  for tag in tags[:5]:
    logging.info('Generating group from tag: {}'.format(tag.name))
    if tag.release:
      release = project.releases.get(tag.release['tag_name'])

      date_time_obj = arrow.get(tag.commit['created_at'])
      dateStr = '{} {}'.format(date_time_obj.date().strftime('%A, %d %B %Y'), date_time_obj.time())

      downloadsFile.write(
        '  <div class="section">\n'
        '    <div class="row center">\n'
        '      <h4 class="header col s12 light">{}</h4>\n'.format(tag.name)
      )
      
      downloadsFile.write(
        '      <h5 class="header col s12 light">{}</h5>\n'.format(dateStr)
      )

      downloadsFile.write(
        '    </div>\n'
        '    <div class="row left">\n'
      )

      changes = tag.commit['message'].split('\n')
      for change in changes:
        if change.startswith(' - '):
          downloadsFile.write('      <p class="light">{}</p>\n'.format(change))
      
      downloadsFile.write(
      '    <div class="row center"></div>\n'
      '    <div class="row center">\n')

      for file in release.assets['links']:
        url = urlparse(file['url'])
        fileName = file['name']

        downloadsFile.write('    <a href="{}" id="download-button" class="btn-small waves-effect waves-light teal lighten-1">{}</a>\n'.format(file['url'], fileName))

      downloadsFile.write('    </div>\n')

      downloadsFile.write(
        '    </div>\n'
        '  </div>\n'
      )

  downloadsFile.write(
  '  <div class="section">\n'
  '    <div class="row center"></div>\n'
  '    <div class="row center">\n'
  '      <a href="{{ site.download_url }}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Full List</a>\n'
  '    </div>\n'
  '  </div>\n'
  '</div>\n'
  )


  downloadsFile.close()


if __name__ == "__main__":
  main()
